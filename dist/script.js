var tempDocData = [
    {
        id: 100, type: 0, formatedText: [], style: {
            fontSize: 16,
            fontFamily: 'Kenia',
            bold: false,
            italic: false,
            fontColor: "#001"
        },
        plainContent: "Sarah: Alex, we need some groceries from the store. Could you please go and get them?",
    }
]
var editor = new ADocEditor({
    container: document.getElementById("user-container-for-editor")
})
setTimeout( ()=>{
    editor.focusOnPage()
}, 100)


var extractedData = null
function getDocumentData(){
    let data = editor.getDocumentData()
    console.log(data)
    localStorage.setItem('oldData', JSON.stringify( data ))
    extractedData = data
    return data
}

function log() {
    editor.log()
}

function setData() {
    if (!extractedData) extractedData = JSON.parse( localStorage.getItem('oldData') )
    if(extractedData){
        editor.setDocumentData(extractedData)
        editor.focusOnPage()
    }
}
setTimeout( ()=>{
    setData()
}, 500 )

function generatePDF() {
    editor.generatePDF("your_doc-pdf-lib.pdf")
}


// editor.addFonts(["./assets/fonts/Caveat-VariableFont_wght.woff2", "./assets/fonts/Caveat-VariableFont_wght.ttf"], 'Caveat')
// editor.addFonts(["./assets/fonts/Comfortaa-VariableFont_wght.woff2", "./assets/fonts/Comfortaa-VariableFont_wght.ttf"], 'Comfortaa')
// editor.addFonts(["./assets/fonts/FiraSans-Regular.woff2", "./assets/fonts/FiraSans-Regular.ttf"], 'Fira Sans')
// editor.addFonts(["./assets/fonts/Kanit-Regular.woff2", "./assets/fonts/Kanit-Regular.ttf"], 'Kanit')
// editor.addFonts(["./assets/fonts/Merriweather-Regular.woff2", "./assets/fonts/Merriweather-Regular.ttf"], 'Merriweather')
// editor.addFonts(["./assets/fonts/Montserrat-VariableFont_wght.woff2", "./assets/fonts/Montserrat-VariableFont_wght.ttf"], 'Montserrat')
// editor.addFonts(["./assets/fonts/Mulish-VariableFont_wght.woff2", "./assets/fonts/Mulish-VariableFont_wght.ttf"], 'Mulish')
// editor.addFonts(["./assets/fonts/NotoSans-VariableFont_wdth,wght.woff2", "./assets/fonts/NotoSans-VariableFont_wdth,wght.ttf"], 'Noto Sans')
// editor.addFonts(["./assets/fonts/Nunito-VariableFont_wght.woff2", "./assets/fonts/Nunito-VariableFont_wght.ttf"], 'Nunito')
// editor.addFonts(["./assets/fonts/OpenSans-VariableFont_wdth,wght.woff2", "./assets/fonts/OpenSans-VariableFont_wdth,wght.ttf"], 'Open Sans')
// editor.addFonts(["./assets/fonts/Pacifico-Regular.woff2", "./assets/fonts/Pacifico-Regular.ttf"], 'Pacifico')
// editor.addFonts(["./assets/fonts/Poppins-Regular.woff2", "./assets/fonts/Poppins-Regular.ttf"], 'Poppins')
// editor.addFonts(["./assets/fonts/Prompt-Regular.woff2", "./assets/fonts/Prompt-Regular.ttf"], 'Prompt')
// editor.addFonts(["./assets/fonts/Rajdhani-Regular.woff2", "./assets/fonts/Rajdhani-Regular.ttf"], 'Rajdhani')
// editor.addFonts(["./assets/fonts/Roboto-Regular.woff2", "./assets/fonts/Roboto-Regular.ttf"], 'Roboto')
// editor.addFonts(["./assets/fonts/Rubik-VariableFont_wght.woff2", "./assets/fonts/Rubik-VariableFont_wght.ttf"], 'Rubik')
// editor.addFonts(["./assets/fonts/SourceCodePro-VariableFont_wght.woff2", "./assets/fonts/SourceCodePro-VariableFont_wght.ttf"], 'Source Code Pro')
// editor.addFonts(["./assets/fonts/Teko-VariableFont_wght.woff2", "./assets/fonts/Teko-VariableFont_wght.ttf"], 'Teko')
// editor.addFonts(["./assets/fonts/Ubuntu-Regular.woff2", "./assets/fonts/Ubuntu-Regular.ttf"], 'Ubuntu')
// editor.addFonts(["./assets/fonts/WorkSans-Regular.woff2", "./assets/fonts/WorkSans-Regular.ttf"], 'Work Sans')
// editor.addFonts(["./assets/fonts/Afacad-VariableFont_wght.woff2", "./assets/fonts/Afacad-VariableFont_wght.ttf"], 'Afacad')
// editor.addFonts(["./assets/fonts/Bitter-VariableFont_wght.woff2", "./assets/fonts/Bitter-VariableFont_wght.ttf"], 'Bitter')


// end of file