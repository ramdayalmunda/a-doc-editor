var counter = 0; // counter is used to generate unique ids. and everytime a newId is assigned counter is increased, ++counter.

var defaultConfig = {
    element: "",
    pageSetup: {
        // mm // these are in mm. //this is for user to see or when PDF will be required
        width: 210,
        height: 297,

        // these will be calculated with the abloeve value so that the content is sharp and clearly visible to the user on browser UI
        canvasMultiplier: 6, // these are calculated so to ensure that the canvas rendering is always HD on any resolution.
        canvasWidth: 210,
        canvasWidth: 297,
    },
    format: {
        background: "#fff",
        margin: 20, // mm //
        border: "", // could be image or anything
    },
    style: {
        fontSize: 70,
        fontFamily: 'Arial',
        bold: false,
        italic: false,
        fontColor: "#1e1f77"
    },
}

var caretData = {
    activeData: dataSet[0], // this holds the active data from the dataSet
    index: 0,// this holds the caret index of the character in that activeData
    interval: null, // to store the setInterval for the blinking cursor animation
    intervalDuration: 800, // ms // to detemine for how long the caret is blinked before reappearing again
    blink: false, // this will keep alternating to to show the caret or not
    canvasIndex: 0, // to store index of the canvas which isBeing edited right now.
    caretSize: config.style.fontSize,
}

var dataType = [
    "plainText", // plain text, normal text in a pdf or doc text
    "list", // items displayed in a list format
]

// dataSet will hold the plain data. This will help us traverse the.
var dataSet = [
    {
        formatedText: [],// it holds the ojects storing the style of each character
        type: 0, // type 0, denotes it is plain text,
        tabNumber: 0, // tab number by default is zero
        plainText: "This is the plain text. This will be used to manage and handle the caret position on the canvas UI",
    },
    {
        formatedText: [],  // if this is blank the default style is used from "config.style"
        type: 1, // type 1, denotes it is a list,
        listType: "",
        tabNumber: 0, // tab number by default is zero
        plainText: "This is the first item in the list. it can be wrapped around the line if necessary"
    },
    {
        formatedText: [],
        // this is the second item on the list because the prvious item is also a list and has the same tabNumber
        type: 1, // type 1, denotes it is a list,
        itemType: "",
        tabNumber: 0, // tab number by default is zero
        plainText: "The second item on the list is here."
    },
    {
        formatedText: [],
        type: 0, // this is just a blank line that is added
        plainText: "",
    },
]

// external Media set
var mediaAlignmentOptions = [
    "In line", // the image will be considered as an character, // can be placed in between a word// dragging will show the new caret position.
    "Wrap text", // image will cause the text to wrap around it,
    "Break text", // image be placed between the lines, not affecting any other thing other than their y position.
    "Behind text", // image will be placed behind text,
    "In front of text", // image will be placed in front of the text,
]
var mediaSet = [
    {
        url: `${window.origin}/assets/img.jpg`,
        style: {
            dataSetIndex: 1, // meaning this belongs to the dataSet[1]
            plainIndex: 100, // meaning this should be rendered after the 100th characters
            alignment: 0, // this means// mediaAlignmentOptions[0]; which is "In line"
            x: 100, // px // this will not work if alignment in 0
            y: 100, // px // this will not work if alignment in 0
            width: 200, // px
            height: 200, // px
        }
    }
]

// this will hold data related to the canvas that is shown on the browser UI
var canvasList = [
    {
        id: ++counter, // everytime a canvas in created it is given some id like this,
        el: document.createElement('canvas'), // this hold the canvas DOM element.
        startingLineIndex: 0, // this tell what line the page starts from drawing.
    }
]

// lines // this holds the array of each line //
var lines = [
    { // this is the 1st line
        "x": 120, // starting position of the 1st character on the line
        "y": 150, // y coordinate of the entire line
        "plainContent": "Hey there buddy! Can you bring these items from the groceries please. It will ",
        "fontSize": 30, // fontSize of the first Character
        "maxFontSize": 30, // maxFont size present in the line; Will be used when selcting texts in line and its distance from the previous line
        "dataSetIndex": 0, // it hold the index of the "dataSet"
        "charStartIndex": 0, // the index of the character in the dataSet which is 1st in the line
        "charEndIndex": 77, // the index of the character in the dataSet which is last in the line
        "type": 0, // type: it denotes the type of the line ie, paragraph, list
        "blockStart": true, // meaning this is the first line of the long set of sentences.
        "maxLineWidth": 1020, // width of the line after which the content would wrap to the next line
        "canvasIndex": 0, // the index of the canvas on which this line would be rendered
    },
    { // this is the second line and so one
        "x": 120,
        "y": 180,
        "plainContent": " be really helpfull",
        "fontSize": 30,
        "maxFontSize": 30,
        "dataSetIndex": 0,
        "charStartIndex": 77,
        "charEndIndex": 96,
        "maxLineWidth": 1020,
        "canvasIndex": 0
    }
    // /// ............
]

