let isModule = (typeof module != 'undefined') ? true : false
if (!isModule) console.warn('Browser Environment')
var ADocEditor = function (customConfig) {
    var container, counter = 0, shadow, pxMmRatio, config, htmlStr, htmlTag, htmlObj = {}, fontList = [];
    const mmPtRatio = 254 / 720; // this is used for font size
    var paperSizes = {
        "A4": { mmWidth: 210, mmHeight: 297 },
    }
    const letters = { "0": "a", "1": "b", "2": "c", "3": "d", "4": "e", "5": "f", "6": "g", "7": "h", "8": "i", "9": "j", "a": "k", "b": "l", "c": "m", "d": "n", "e": "o", "f": "p", "g": "q", "h": "r", "i": "s", "j": "t", "k": "u", "l": "v", "m": "w", "n": "x", "o": "y", "p": "z" }
    const romanNumerals = [["I", "IV", "V", "IX"], ["X", "XL", "L", "XC"], ["C", "CD", "D", "CM"], ["M"]];

    const textStyleList = [
        { type: "Normal Text", fontFamily: "Libre Franklin", fontSize: 11, fontColor: "#000", bold: false, italic: false, spaces: 10 },
        { type: "Title", fontFamily: "Libre Franklin", fontSize: 26, fontColor: "#000", bold: false, italic: false, spaces: 0 },
        { type: "Subtitle", fontFamily: "Libre Franklin", fontSize: 15, fontColor: "#555", bold: false, italic: false, spaces: 4 },
        { type: "Heading 1", fontFamily: "Libre Franklin", fontSize: 20, fontColor: "#000", bold: false, italic: false, spaces: 2 },
    ]
    const styleName = {};
    textStyleList.forEach((style, s) => { styleName[style.type] = s })

    const listStyles = [
        { type: "Number", value: 0 },
        { type: "Bullet", value: 1 },
    ]


    var defaultConfig = {
        pageSetup: { size: "A4" }, zoom: 1,
        format: {
            background: "#fff",
            margin: 15, // mm
            border: "",
            tabWidth: 10, // mm
        },
        style: {
            type: textStyleList[0].type,
            // fontSize: 11, // this is in pt // 1pt = 1/72 inch // 1pt = 254/72 mm
            // fontFamily: 'Libre Franklin',
            // bold: false,
            // italic: false,
            // fontColor: "#f01",
        },
    }
    config = JSON.parse(JSON.stringify(defaultConfig))
    var dataList = [
        {
            id: ++counter,
            type: 0,
            formatedText: [],
            plainContent: "",
            tabCount: 0,
            style: { ...config.style },
            listItemNumber: 0,
            // newPage: false, // if this is true the data is send to new page
        },
    ]

    var caretData = {
        activeData: dataList[0],
        index: 0,
        timeout: null,
        timeoutDuration: 800,
        blink: true,
        pageIndex: 0,
        caretSize: config.style.fontSize,
        previousCaret: null,
        style: { type: "Normal Text" },
        dimension: {
            x: config.format.margin,
            y: config.format.margin + (3 * config.style.fontSize / 4)
        },
        dragging: {
            is: false, // to check if mouse is down for dragging,
            started: false, // to check if the mouse has moved for dragging
            startX: null, // start and end are coordinated of opposite corner
            startY: null,
            endX: null,
            endY: null,
            startingDataIndex: null,
            endDataIndex: null,
            startingCharIndex: null,
            endIndex: null,
        }
    }
    var upDownArrowX = null
    var pageList = []
    var pageScrollingDiv = null
    var focussedPage = null
    var lastFocussedPage = null
    var isRendering = false
    var isSelectionRendering = false
    function initialize() {
        config = JSON.parse(JSON.stringify(defaultConfig))
        container = customConfig.container
        shadow = container.attachShadow({ mode: "open" })

        htmlStr = /*html*/`
<div class="header">
    <div class="toolbar">
        <div class="item" adc-target="file-menu" adc-type="menu-item">
            <span>File</span>
            <div adc="file-menu" adc-type="menu-options">
                <div adc-type="sub-menu" adc-action="new-file">New</div>
                <div adc-type="sub-menu" adc-action="open-file">Open</div>
                <div adc-type="sub-menu" adc-action="save-file">Save</div>
                <div adc-type="sub-menu" adc-action="print-file">Print</div>
            </div>
        </div>
        <div class="item" adc-target="help-menu" adc-type="menu-item">
            <span>Help</span>
            <div adc="help-menu" adc-type="menu-options">
                <div adc-type="sub-menu" adc-action="about">About</div>
                <div adc-type="sub-menu" adc-action="tutorial">Tutorial</div>
            </div>
        </div>

    </div>
    <div class="toolbar">

        <div class="item" adc="font-family"></div>
        <div class="item" adc="list-style"></div>
        <div class="item">
            <button adc="font-size-change" class="small-btn" value="-1">-</button>
            <input type="number" adc="font-size-input" class="small-input">
            <button adc="font-size-change" class="small-btn" value="+1">+</button>
        </div>
        <div class="item" adc="style-handler"></div>
        
    </div>
</div>

<div class="content-area">
    <div class="left-sidebar hide">
        <button class="toggle-btn rotate" adc="toggle-left-sidebar">&#10554;</button>
        <div class="content" adc="left-sidebar-content"></div>
    </div>
    <div class="page-list" adc="page-list"></div>

    <!-- <div class="right-sidebar hide">
        <button class="toggle-btn" adc="toggle-right-sidebar">X</button>
        <div class="content" adc="right-sidebar-content"></div>
    </div> -->
</div>
<div class="footer">

    <div class="toolbar">
        <select class="item" title="Select zoom" adc="zoom">
            <option value="" disabled>Select Zoom</option>
            <option value="0.1">10%</option>
            <option value="0.2">20%</option>
            <option value="0.3">30%</option>
            <option value="0.4">40%</option>
            <option value="0.5">50%</option>
            <option value="0.6">60%</option>
            <option value="0.7">70%</option>
            <option value="0.8">80%</option>
            <option value="0.9">90%</option>
            <option value="1" selected>100%</option>
            <option value="1.2">120%</option>
            <option value="1.5">150%</option>
            <option value="1.8">180%</option>
            <option value="2">200%</option>
        </select>
        <span class="item">Words : 0</span>
        <span class="item">Sentences : 0</span>
        <span class="item">Pages : 1</span>
    </div>
</div>
<hidden>
    <a adc="download-link">
    <input adc="open-file" type="file" accept=".json"/>
</hidden>
        `;


        let styleSheets = customConfig?.styleSheet?.length ? customConfig.styleSheet : []
        styleSheets.splice(0, 0, "/assets/a-doc-editor.css")
        for (let i = 0; i < styleSheets.length; i++) {
            let link = document.createElement('link')
            link.setAttribute('rel', 'stylesheet');
            link.setAttribute('href', styleSheets[i]);
            shadow.append(link)
        }

        htmlTag = document.createElement('div')
        htmlTag.setAttribute('class', 'main')
        htmlTag.innerHTML = htmlStr
        shadow.append(htmlTag)

        htmlObj = {
            header: shadow.querySelector('.header'),
            pageList: shadow.querySelector('[adc="page-list"]')
        }
        pageScrollingDiv = shadow.querySelector('[adc="page-list"]')


        !(function zoomHandles() {
            let zoomSelect = shadow.querySelector('[adc="zoom"]')
            zoomSelect.addEventListener('change', (e) => {
                config.zoom = Number(e.target.value)
                reConfigure(config)
            })
        })()

        !(function fontSelectHandle() {
            addFonts(["./assets/fonts/ArchitectsDaughter-Regular.woff2", "./assets/fonts/ArchitectsDaughter-Regular.ttf"], 'Architects Daughter')
            addFonts(["./assets/fonts/Assistant-VariableFont_wght.woff2", "./assets/fonts/Assistant-VariableFont_wght.ttf"], 'Assistant')
            addFonts(["./assets/fonts/CrimsonText-Regular.woff2", "./assets/fonts/CrimsonText-Regular.ttf"], 'Crimson Text')
            addFonts(["./assets/fonts/EduTASBeginner-VariableFont_wght.woff2", "./assets/fonts/EduTASBeginner-VariableFont_wght.ttf"], 'Edu TAS Beginner')
            addFonts(["./assets/fonts/IndieFlower-Regular.woff2", "./assets/fonts/IndieFlower-Regular.ttf"], 'Indie Flower')
            addFonts(["./assets/fonts/Karla-VariableFont_wght.woff2", "./assets/fonts/Karla-VariableFont_wght.ttf"], 'Karla')
            addFonts(["./assets/fonts/Kenia-Regular.woff2", "./assets/fonts/Kenia-Regular.ttf"], 'Kenia')
            addFonts(["./assets/fonts/Lato-Regular.woff2", "./assets/fonts/Lato-Regular.ttf"], 'Lato')
            addFonts(["./assets/fonts/LibreFranklin-VariableFont_wght.woff2", "./assets/fonts/LibreFranklin-VariableFont_wght.ttf"], 'Libre Franklin')
            addFonts(["./assets/fonts/Lora-Regular.woff2", "./assets/fonts/Lora-Regular.ttf"], 'Lora')
            addFonts(["./assets/fonts/Macondo-Regular.woff2", "./assets/fonts/Macondo-Regular.ttf"], 'Macondo')
            addFonts(["./assets/fonts/Outfit-VariableFont_wght.woff2", "./assets/fonts/Outfit-VariableFont_wght.ttf"], 'Outfit')
            addFonts(["./assets/fonts/TitilliumWeb-Regular.woff2", "./assets/fonts/TitilliumWeb-Regular.ttf"], 'Titillium Web')
            addFonts(["./assets/fonts/VarelaRound-Regular.woff2", "./assets/fonts/VarelaRound-Regular.ttf"], 'Varela Round')
        })()

        !(function handleList() {
            // handle list
            let listStyleElem = shadow.querySelector('[adc="list-style"]')
            listStyleElem.innerHTML = ""

            let selection = document.createElement('span')
            selection.setAttribute('value', null)
            selection.setAttribute('adc-type', 'selection')
            selection.innerHTML = "[No-List]"
            listStyleElem.append(selection)

            let ul = document.createElement('ul')
            ul.setAttribute('class', 'dropdown')
            listStyleElem.append(ul)

            listStyles.forEach(item => {
                let option = document.createElement('li')
                option.setAttribute('value', item.value)
                option.innerHTML = item.type
                ul.append(option)
                option.addEventListener('click', (e) => {
                    if (caretData.activeData) {
                        caretData.activeData.type = (caretData.activeData.type == 1 && caretData.activeData.listStyle == item.value) ? 0 : 1
                        if (caretData.activeData.type == 1) {
                            caretData.activeData.listStyle = item.value
                        } else {
                            e.stopPropagation()
                            ul.classList.remove('show')
                            selection.innerHTML = "[No-List]"
                        }
                        changeListStyle()
                        caretData.blink = false
                        reRenderCanvas()
                        focusOnPage()
                    }
                })
            })
        })()

        !(function fontSizeHandler() {
            let fontSizeInput = shadow.querySelector('[adc="font-size-input"]')
            let caretStyle = getFullStyle(caretData.style)
            fontSizeInput.value = caretStyle.fontSize
            let fontSizeChangers = shadow.querySelectorAll('[adc="font-size-change"]')
            fontSizeChangers.forEach(btn => {
                btn.addEventListener('click', (e) => {

                    let caretStyle = getFullStyle(caretData.style)
                    let change = Number(e.target.getAttribute('value'))
                    change = change ? change : 0
                    change = caretStyle.fontSize + change
                    change = change ? change : caretStyle.fontSize

                    caretData.style = {
                        ...caretStyle,
                        fontSize: change,
                    }
                    delete caretData.style.type
                    fontSizeInput.value = change
                    focusOnPage()
                })
            })

            function changeFontEvent(e) {
                let value = Number(e.target.value)
                let caretStyle = getFullStyle(caretData.style)
                value = value ? value : config.style.fontSize
                caretData.style = {
                    ...caretStyle,
                    fontSize: value,
                }
                delete caretData.style.type
                if (e?.key == 'Enter') focusOnPage()
            }
            fontSizeInput.addEventListener('keydown', changeFontEvent)
            fontSizeInput.addEventListener('input', changeFontEvent)

        })()

        !(function menuHandler() {
            shadow.querySelector('[adc-action="new-file"]')
                .addEventListener('click', newDocument)

            shadow.querySelector('[adc-action="save-file"]')
                .addEventListener('click', downloadJSON)

            let openFileInput = shadow.querySelector('[adc="open-file"]')
            openFileInput.addEventListener('change', (e) => {
                let file = e.target?.files?.[0]
                if (file) {
                    const reader = new FileReader()
                    reader.onload = function (e) {
                        const fileContent = e.target.result;
                        let jsonData = JSON.parse(fileContent)
                        setDocumentData(jsonData)
                    };
                    reader.readAsText(file);
                }
            })
            shadow.querySelector('[adc-action="open-file"]')
                .addEventListener('click', () => { openFileInput.click() })

            shadow.querySelector('[adc-action="print-file"]')
                .addEventListener('click', printDoc)
        })()

        !(function styleHandler() {
            let textStyleElem = shadow.querySelector('[adc="style-handler"]')
            textStyleElem.innerHTML = ""

            let selectionElem = document.createElement('span')
            selectionElem.setAttribute('adc-type', 'selection')
            if (caretData.style.type) selectionElem.innerHTML = caretData.style.type
            else selectionElem.innerHTML = "[CUSTOM]"
            textStyleElem.append(selectionElem)

            let ul = document.createElement('ul')
            ul.setAttribute('class', 'dropdown')
            textStyleElem.append(ul)

            textStyleList.forEach((style, s) => {
                let option = document.createElement('li')
                option.setAttribute('value', style.type)
                option.append(style.type)
                option.style.fontFamily = style.fontFamily
                option.style.fontSize = `${style.fontSize}pt`
                option.style.color = style.fontColor
                ul.append(option)
                option.addEventListener('click', (e) => {
                    if (caretData.activeData) {
                        caretData.activeData.style = { type: style.type }
                        for (let c = 0; c < caretData.activeData.formatedText.length; c++) {
                            caretData.activeData.formatedText[c].style = { type: style.type }
                        }
                        reRenderCanvas()
                        focusOnPage()
                        caretData.style = { type: style.type }
                        caretData.blink = false
                        renderCaret(true)
                    }
                    setupHeaders()
                })
            })
        })()

        !(function sidebarsHandlers() {
            let leftSidebar = shadow.querySelector('.left-sidebar')
            let toggleLeft = shadow.querySelector('[adc="toggle-left-sidebar"]')
            toggleLeft.addEventListener('click', () => {
                leftSidebar.classList.toggle('hide')
                toggleLeft.classList[leftSidebar.classList.contains('hide') ? 'add' : 'remove']('rotate')
            })

            // let rightSidebar = shadow.querySelector('.right-sidebar')
            // let toggleRight = shadow.querySelector('[adc="toggle-right-sidebar"]')
            // toggleRight.addEventListener('click', ()=>{
            //     rightSidebar.classList.toggle('hide')
            //     toggleRight.scrollIntoView({behavior: 'smooth'})
            // })
        })()


        reConfigure(customConfig)
        reRenderCanvas()
        addGlobalEvents()
    }


    function reConfigure(newConfig) {
        if (newConfig?.size && paperSizes[newConfig?.size]) { config.pageSetup = { ...paperSizes[newConfig.size], size: newConfig.size } }
        else if (newConfig?.width && newConfig?.height) {
            config.pageSetup = { size: "Custom", mmWidth: newConfig.width, mmHeight: newConfig.height }
        }
        else { config.pageSetup = { size: "A4", ...paperSizes['A4'] } }

        if (typeof window == 'object') {
            config.pageSetup.multipler = config.pageSetup.mmHeight / config.pageSetup.mmWidth
            config.pageSetup.pxWidth = 2480
            config.pageSetup.pxHeight = Math.ceil(config.pageSetup.pxWidth * config.pageSetup.multipler)
        } else {
            config.pageSetup.pxHeight = config.pageSetup.mmHeight
            config.pageSetup.pxWidth = config.pageSetup.mmWidth
        }

        if (config.zoom) {
            config.pageSetup.uiWidth = config.pageSetup.mmWidth * config.zoom
            config.pageSetup.uiHeight = config.pageSetup.mmHeight * config.zoom
        }

        for (let p = 0; p < pageList.length; p++) {
            pageList[p].el.style.width = `${config.pageSetup.uiWidth}mm`
            pageList[p].selectionCanvas.style.width = `${config.pageSetup.uiWidth}mm`
            pageList[p].page.style.width = `${config.pageSetup.uiWidth}mm`
            pageList[p].page.style.height = `${config.pageSetup.uiHeight}mm`
        }

        pxMmRatio = config.pageSetup.pxWidth / config.pageSetup.mmWidth;
        return config
    }

    function reRenderCanvas() {
        if (isRendering) return
        let pageIndex = 0
        if (!pageList.length) {
            let newPage = createNewPage()
            pageList[pageIndex] = { id: ++counter, el: newPage.el, page: newPage.page, selectionCanvas: newPage.selectionCanvas, dataIndex: 0 }
            shadow.querySelector('[adc="page-list"]').append(pageList[pageIndex].page)
        }


        // to clear the canvases
        for (let i = 0; i < pageList.length; i++) {
            let ctx = pageList[i].el.getContext('2d', { willReadFrequently: true })
            ctx.save()
            ctx.clearRect(0, 0, pageList[i].el.width, pageList[i].el.height); // clears the canvas
            ctx.fillStyle = config.format.background
            ctx.fillRect(0, 0, pageList[i].el.width, pageList[i].el.height)
            ctx.restore()
        }


        lines = []

        setupHeaders(); // this is async function //
        calculateTextSizeAndPosition()
        renderLines()
        caretData.previousCaret = null
        renderCaret(true)

        function calculateTextSizeAndPosition() {
            let d = 0, c = 0;
            function getLineObj() {
                let newLineObj = {
                    ...getFullStyle(config.style),
                    x: 0, // this is the starting point x; it will change based on the tabNumber
                    y: 0, // this is the starting y coordinate; it will change based on the max font size
                    plainContent: "",
                    dataIndex: d,
                    startingCharIndex: 0, // index from where to check
                    endingCharIndex: 0, // index till where to check// not including this index.
                    listItemNumber: 0,
                }
                newLineObj.maxFontSize = newLineObj.fontSize
                return newLineObj
            }
            for (d = 0; d < dataList.length; d++) {
                let lineObj = getLineObj()
                let dataBlock = dataList[d]
                let canvas = pageList[pageIndex].el
                let ctx = canvas.getContext('2d', { willReadFrequently: true })
                ctx.save()

                // to calculate the lines
                let tempLineWidth = 0;
                let maxLineWidth = config.pageSetup.pxWidth - (config.format.margin * pxMmRatio * 2)
                let tabWidth = ((dataBlock.type == 1 ? 1 : 0) + dataBlock.tabCount) * config.format.tabWidth * pxMmRatio
                lineObj.tabWidth = tabWidth
                maxLineWidth -= tabWidth
                lineObj.dataType = dataBlock.type
                let wordEndIndex = 0; // this stores the index of the word which can fit in the line;
                let tempWordWidth = 0
                if (dataBlock.type == 1) {
                    let previousBlock = dataList[d - 1]
                    if (!previousBlock || previousBlock?.type != 1) dataBlock.listItemNumber = 1
                    else if (previousBlock.tabCount == dataBlock.tabCount) {
                        dataBlock.listItemNumber = previousBlock.listItemNumber + 1
                    } else {
                        dataBlock.listItemNumber = 1
                        let olderBlockIndex = d - 1
                        while (dataList?.[olderBlockIndex]?.type == 1 && dataList?.[olderBlockIndex]?.tabCount >= dataBlock.tabCount) {
                            if (dataList?.[olderBlockIndex]?.tabCount == dataBlock.tabCount) {
                                dataBlock.listItemNumber = dataList?.[olderBlockIndex].listItemNumber + 1
                                break;
                            }
                            --olderBlockIndex;
                        }
                    }
                }
                for (c = 0; c < dataBlock.plainContent.length; c++) {
                    let style = getFullStyle(dataBlock?.formatedText?.[c].style)
                    lineObj.maxFontSize = (lineObj.maxFontSize < style.fontSize) ? style.fontSize : lineObj.maxFontSize

                    if (/\s/.test(dataBlock.plainContent[c])) {
                        // condition to check if a blank character is found.
                        wordEndIndex = c
                        lineObj.endingCharIndex = c
                        tempWordWidth = 0
                    }
                    let charWidth = getCharacterWidth(dataBlock.plainContent[c], style)

                    dataBlock.formatedText[c].dimension.width = charWidth

                    // // if with the current char the string could not fit on one line
                    if (tempLineWidth + charWidth > maxLineWidth) {
                        // cannot add this// new line should be added//

                        if (tempWordWidth + charWidth >= maxLineWidth) {
                            // this is to manage the casse where there is a set of long string without any blank space. In that case the text is broken and moved to next line.

                            wordEndIndex = c
                            lineObj.endingCharIndex = c
                            tempWordWidth = 0
                        }

                        lineObj.plainContent = dataBlock.plainContent.slice(lineObj.startingCharIndex, lineObj.endingCharIndex + 1)
                        lineObj.maxLineWidth = maxLineWidth
                        lines.push(lineObj)
                        lineObj = new getLineObj()
                        lineObj.maxFontSize = style.fontSize
                        if (dataBlock.type == 1) {
                            lineObj.tabWidth = tabWidth
                            lineObj.tabCount = dataBlock.tabCount
                        } else {
                            maxLineWidth = config.pageSetup.pxWidth - (config.format.margin * pxMmRatio * 2)
                            lineObj.tabWidth = 0
                            lineObj.tabCount = 0
                        }
                        lineObj.startingCharIndex = wordEndIndex + 1
                        lineObj.endingCharIndex = wordEndIndex + 1
                        tempLineWidth = tempWordWidth
                        lineObj.maxLineWidth = maxLineWidth
                    }
                    else {
                        // if the char can fit in the same line. then it is well and good
                        tempLineWidth += charWidth
                        tempWordWidth += charWidth
                    }


                }

                lineObj.plainContent = dataBlock.plainContent.slice(lineObj.startingCharIndex, lineObj.endingCharIndex + 1)
                // there is chance that the last line is not at the width// so we need to handle the last line separately
                if (lineObj.endingCharIndex <= dataBlock.plainContent.length) {
                    lineObj.endingCharIndex = dataBlock.plainContent.length
                }
                lineObj.plainContent = dataBlock.plainContent.slice(lineObj.startingCharIndex, lineObj.endingCharIndex)

                lines.push(lineObj)

                ctx.restore()
            }
            return

        }

        function renderLines() {
            let maxHeight = config.pageSetup.pxHeight - config.format.margin * pxMmRatio * 2
            let pageIndex = 0
            let x = 0, y = config.format.margin * pxMmRatio;
            let firstLineOnPage = true
            for (let l = 0; l < lines.length; l++) {
                let setData = dataList[lines[l].dataIndex]
                let ctx = pageList[pageIndex].el.getContext('2d', { willReadFrequently: true })
                x = config.format.margin * pxMmRatio
                x += lines[l].tabWidth
                y = y + (lines[l].maxFontSize * pxMmRatio * mmPtRatio)
                lines[l].y = y


                // check if the line is to be written on the next page
                if (maxHeight < y) {
                    pageIndex++
                    if (!pageList?.[pageIndex]) {
                        let newPage = createNewPage()
                        pageList[pageIndex] = { id: ++counter, el: newPage.el, page: newPage.page, selectionCanvas: newPage.selectionCanvas, dataIndex: lines[l].dataIndex }
                        shadow.querySelector('[adc="page-list"]').append(pageList[pageIndex].page)
                    }
                    ctx = pageList[pageIndex].el.getContext('2d', { willReadFrequently: true })
                    y = config.format.margin * pxMmRatio + (lines[l].maxFontSize * pxMmRatio * mmPtRatio);
                    lastFocussedPage = pageList[pageIndex].el
                    caretData.blink = false
                    focusOnPage({ scroll: true })
                    firstLineOnPage = true
                }
                lines[l].pageIndex = pageIndex


                ctx.save()
                if (!firstLineOnPage) {
                    y = y + ((lines[l - 1].maxFontSize / 3) * pxMmRatio * mmPtRatio)
                    lines[l].y = y
                } else {
                    pageList[pageIndex].startingLineIndex = l
                    pageList[pageIndex].startingDataIndex = lines[l].dataIndex
                    pageList[pageIndex].startingCharIndex = lines[l].startingCharIndex
                }
                lines[l].apparentY = y

                // this is to render the numbering and bullets etc
                if (lines[l].dataType == 1) {
                    let setStyle = getFullStyle(setData.style)
                    ctx.save()
                    ctx.font = `${setStyle.fontSize * pxMmRatio * mmPtRatio}px ${setStyle.fontFamily}`
                    ctx.fillStyle = `${setStyle.fontColor}`
                    let label = getLabel(lines[l].dataIndex)
                    let labelWidth = ctx.measureText(label).width
                    labelWidth += mmPtRatio * pxMmRatio * 5
                    lines[l].labelWidth = labelWidth
                    ctx.fillText(label, x - labelWidth, y)
                    ctx.restore()
                }



                for (let c = lines[l].startingCharIndex; c <= lines[l].endingCharIndex; c++) {
                    let char = setData?.plainContent[c]
                    if (char) {
                        let style = getFullStyle(setData.formatedText[c].style)
                        ctx.save()
                        ctx.font = `${style?.bold ? 'bold ' : ''}${style?.italic ? 'italic ' : ''} ${style.fontSize * pxMmRatio * mmPtRatio}px ${style.fontFamily}`
                        ctx.fillStyle = `${style?.fontColor}`
                        ctx.fillText(char, x, y)
                        setData.formatedText[c].dimension.x = x
                        setData.formatedText[c].dimension.y = y
                        ctx.restore()
                        if (setData.formatedText[c]?.dimension?.width) {
                            x += setData.formatedText[c]?.dimension?.width
                        }
                    }
                }

                ctx.restore()
                firstLineOnPage = false
            }
            return
        }


        function getCharacterWidth(char, style) {
            let canvas = pageList[0].el
            let ctx = canvas.getContext('2d', { willReadFrequently: true })
            ctx.save()
            ctx.font = `${style?.bold ? 'bold ' : ''}${style?.italic ? 'italic ' : ''} ${style.fontSize * pxMmRatio * mmPtRatio}px ${style.fontFamily}`
            ctx.fillStyle = `${style?.fontColor}`
            let width = ctx.measureText(char).width
            ctx.restore()
            return width
        }


        function createNewPage() {
            let page, canvas, selectionCanvas;
            if (isModule) {
                const { createCanvas } = require('canvas')
                canvas = createCanvas(config.pageSetup.canvasWidth, config.pageSetup.canvasHeight)
            } else {
                canvas = document.createElement('canvas')
                canvas.setAttribute('class', 'page')
                canvas.width = config.pageSetup.canvasWidth
                canvas.height = config.pageSetup.canvasHeight
                canvas.setAttribute("tabIndex", -1)
                canvas.setAttribute('tabIndex', '-1')
                canvas.width = config.pageSetup.pxWidth
                canvas.height = config.pageSetup.pxHeight

                canvas.style.width = `${config.pageSetup.uiWidth}mm`

                let ctx = canvas.getContext('2d', { willReadFrequently: true })
                ctx.fillStyle = config.format.background
                ctx.fillRect(0, 0, canvas.width, canvas.height)

                canvas.addEventListener('keydown', keydownHandler)
                canvas.addEventListener('mousedown', mousedownHandler)
                canvas.addEventListener('mousemove', mouseMoveHandler)
                canvas.addEventListener("focus", onFocusHandler)
                canvas.addEventListener("blur", onBlurHandler)

                selectionCanvas = document.createElement('canvas')
                selectionCanvas.setAttribute('class', 'selection')
                selectionCanvas.width = config.pageSetup.pxWidth
                selectionCanvas.height = config.pageSetup.pxHeight
                selectionCanvas.style.width = `${config.pageSetup.uiWidth}mm`

                page = document.createElement('div')
                page.setAttribute('class', 'page')
                page.style.width = `${config.pageSetup.uiWidth}mm`
                page.style.height = `${config.pageSetup.uiHeight}mm`
                page.append(canvas)
                page.append(selectionCanvas)

            }
            return { page, el: canvas, selectionCanvas }
        }

        function keydownHandler(e) {

            if (e.altKey) return
            else if (e.key == 'Tab') {
                e.preventDefault()
                manageIndentation(e.shiftKey ? -1 : 1)
            } else if ([']', '['].includes(e.key) && e.ctrlKey) {
                manageIndentation(e.key == '[' ? -1 : 1)
            }
            else if (e.key == 'Backspace') {

                if (caretData.index == 0) {
                    let activeDataIndex = dataList.findIndex(item => item.id == caretData.activeData.id)
                    if (caretData.activeData.tabCount) {
                        if (caretData.activeData.type == 1) {
                            caretData.activeData.type = 0
                        } else {
                            caretData.activeData.tabCount--
                        }
                    }
                    else if (activeDataIndex >= 0) {
                        if (caretData.activeData.type == 0) {
                            if (activeDataIndex != 0) {
                                if (!caretData.activeData.plainContent.length) { dataList.splice(activeDataIndex, 1) }
                                caretData.activeData = dataList[activeDataIndex - 1]
                                caretData.index = caretData.activeData.plainContent.length
                            }
                        } else if (caretData.activeData.type == 1) {
                            caretData.activeData.type = 0
                        }
                    }
                } else if (e.ctrlKey || e.metaKey) {

                } else {
                    caretData.activeData.plainContent = caretData.activeData.plainContent.slice(0, caretData.index - 1) + caretData.activeData.plainContent.slice(caretData.index)
                    caretData.activeData.formatedText.splice(caretData.index - 1, 1)
                    --caretData.index;
                }

                setCaretStyle()
                setToolbarData({ ...caretData.style, dataSetType: caretData.activeData.type, listStyle: caretData.activeData.listStyle })
            }
            else if (e.key == 'Enter') {
                let dataObj = {
                    id: ++counter,
                    type: caretData.activeData.type,
                    formatedText: [],
                    plainContent: "",
                    listStyle: caretData.activeData.listStyle,
                    tabCount: caretData.activeData.tabCount,
                    style: { ...caretData.activeData.style }
                }

                dataObj.plainContent = caretData.activeData.plainContent.slice(caretData.index)
                caretData.activeData.plainContent = caretData.activeData.plainContent.slice(0, caretData.index)
                dataObj.formatedText = caretData.activeData.formatedText.splice(caretData.index)

                let activeDataIndex = dataList.findIndex(item => item.id == caretData.activeData.id)
                dataList.splice(activeDataIndex + 1, 0, dataObj)

                if (e.ctrlKey || e.metaKey) dataObj.newPage = true
                caretData.index = 0
                caretData.activeData = dataObj
                if (dataObj.type == 0) dataObj.tabCount = 0
                if (!dataObj.formatedText.length && caretData.style.type) {
                    caretData.style = { type: textStyleList[0].type }
                    caretData.activeData.style = { type: textStyleList[0].type }
                } else {
                    dataObj.style = { ...caretData.style }
                }
                // dataList.push(dataObj)
                setCaretStyle()
                setToolbarData({ ...caretData.style, dataSetType: caretData.activeData.type, listStyle: caretData.activeData.listStyle })
            }
            else if (e.key.length == 1 && !e.ctrlKey && !e.metaKey) { // displayable text
                // ***** DISPLAYABLE TEXT **** //
                e.preventDefault()
                caretData.activeData.plainContent = caretData.activeData.plainContent.slice(0, caretData.index) + e.key + caretData.activeData.plainContent.slice(caretData.index)
                let style = caretData.style
                style = style ? style : null
                caretData.activeData.formatedText.splice(caretData.index, 0, { style: style, dimension: {} })
                ++caretData.index
            }
            else if (e.key == 'ArrowLeft') {
                e.preventDefault()
                if (caretData.index) {
                    --caretData.index
                } else {
                    let dataIndex = dataList.findIndex(item => item.id == caretData.activeData.id)
                    if (dataIndex > 0) {
                        caretData.activeData = dataList[dataIndex - 1]
                        caretData.index = caretData.activeData.plainContent.length
                    }
                }
                upDownArrowX = null
                setCaretStyle()
                setToolbarData({ ...caretData.style, dataSetType: caretData.activeData.type, listStyle: caretData.activeData.listStyle })
                caretData.blink = false
                renderCaret(true)
                return
            }
            else if (e.key == 'ArrowRight') {
                e.preventDefault()
                if (e.shiftKey && !caretData.dragging.is) {
                    caretData.dragging.startingDataIndex = dataList.findIndex(item => item.id == caretData.activeData.id)
                    caretData.dragging.startingCharIndex = caretData.index
                    caretData.dragging.is = true
                } else {
                    stopDragging(true)
                }
                if (caretData.index < caretData.activeData.plainContent.length) {
                    ++caretData.index
                } else {
                    let dataIndex = dataList.findIndex(item => item.id == caretData.activeData.id)
                    if (dataList[dataIndex + 1]) {
                        caretData.activeData = dataList[dataIndex + 1]
                        caretData.index = 0
                    }
                }
                upDownArrowX = null
                setCaretStyle()
                setToolbarData({ ...caretData.style, dataSetType: caretData.activeData.type, listStyle: caretData.activeData.listStyle })
                caretData.blink = false
                renderCaret(true)
                return
            }
            else if (e.key == 'ArrowUp') {
                e.preventDefault()
                let currentX = getCurrentX(true);
                upDownArrowX = currentX

                let dataIndex = dataList.findIndex(item => item.id == caretData.activeData.id)
                let lineIndex = lines.findIndex(item => {
                    return item.dataIndex == dataIndex && item.endingCharIndex >= caretData.index
                })
                if (lineIndex > 0) {
                    placeCaret({
                        dataSetIndex: lines[lineIndex-1].dataIndex,
                        index: getCharIndexByLineAndX(lineIndex-1, currentX)
                    })
                }
                caretData.blink = false
                renderCaret(true)
                return

            }
            else if (e.key == 'ArrowDown') {
                e.preventDefault()
                let currentX = getCurrentX(true);
                upDownArrowX = currentX

                let dataIndex = dataList.findIndex(item => item.id == caretData.activeData.id)
                let lineIndex = lines.findIndex(item => {
                    return item.dataIndex == dataIndex && item.endingCharIndex >= caretData.index
                })
                if (lineIndex < lines.length-1) {
                    placeCaret({
                        dataSetIndex: lines[lineIndex+1].dataIndex,
                        index: getCharIndexByLineAndX(lineIndex+1, currentX)
                    })
                }
                caretData.blink = false
                renderCaret(true)
                return
            }

            function manageIndentation(value) {
                caretData.activeData.tabCount += value
                if (caretData.activeData.tabCount < 0) caretData.activeData.tabCount = 0
                else if (caretData.activeData.tabCount > 5) caretData.activeData.tabCount = 5
                return true
            }

            caretData.blink = false
            caretData.previousCaret = null
            reRenderCanvas()
        }

        function getCurrentX(allowUpDownArrow){
            if (!allowUpDownArrow || upDownArrowX==null){
                let currentX = caretData.activeData.formatedText[caretData.index]?.dimension?.x
                if (!currentX) currentX = caretData.activeData.formatedText[caretData.index - 1]?.dimension?.x
                return currentX
            }else return upDownArrowX
        }

        function getCharIndexByLineAndX( lineIndex, refX ){

            let dataSet = dataList[lines[lineIndex].dataIndex]
            let charIndex = lines[lineIndex].startingCharIndex
            for (let c = lines[lineIndex].startingCharIndex; c <= lines[lineIndex].endingCharIndex; c++) {
                charIndex = c;
                let width = dataSet.formatedText?.[c]?.dimension?.width
                width = width ? width : 0
                let x = dataSet.formatedText?.[c]?.dimension?.x
                x = x ? x : 0
                if (refX <= x + width / 2) break;
            }

            return charIndex
        }

        function getCaretPosition(e, coordinates, pageIndex) {
            let cursor = coordinates
            if (!cursor) cursor = getCoordinates(e)
            if (!pageIndex) pageIndex = pageList.findIndex(item => item.el == e.target)

            let lineIndex = pageList[pageIndex].startingLineIndex
            for (let l = pageList[pageIndex].startingLineIndex; lines[l]?.pageIndex == pageIndex; l++) {
                if (!lines[l]) break
                lineIndex = l
                if (cursor.y <= lines[l].apparentY) break
            }
            let lineObj = lines[lineIndex]
            let dataSet = dataList[lineObj.dataIndex]
            let charIndex = lineObj.startingCharIndex
            for (let c = lineObj.startingCharIndex; c <= lineObj.endingCharIndex; c++) {
                charIndex = c;
                let width = dataSet.formatedText?.[c]?.dimension?.width
                width = width ? width : 0
                let x = dataSet.formatedText?.[c]?.dimension?.x
                x = x ? x : 0
                if (cursor.x <= x + width / 2) break;
            }
            cursor = {
                dataSetIndex: lineObj.dataIndex,
                index: charIndex ? charIndex : 0,
                ...cursor,
            }
            return cursor
        }

        function placeCaret(pos) {

            if (typeof pos.dataSetIndex == 'number' && typeof pos.index == 'number') {
                caretData.activeData = dataList[pos.dataSetIndex]
                caretData.index = pos.index
                caretData.blink = false
                renderCaret(true)
                return
            }
            console.warn('no char found')
        }

        function setDragStartPosition() {
            caretData.dragging.startingDataIndex = dataList.findIndex(item => item.id == caretData.activeData.id)
            caretData.dragging.startingCharIndex = caretData.index
        }

        function getCoordinates(e) {
            const rect = e.target.getBoundingClientRect(); // Get the position of the canvas
            return {
                x: (e.clientX - rect.left) * config.pageSetup.pxWidth / rect.width,
                y: (e.clientY - rect.top) * config.pageSetup.pxHeight / rect.height,
            }
        }


        function mousedownHandler(e) {
            stopDragging(true)
            if (focussedPage) {
                e.stopPropagation()
                let caretPos = getCaretPosition(e)
                placeCaret(caretPos)
                let style = caretData.activeData.formatedText[caretData.index]?.style
                if (!style) style = caretData.activeData.formatedText[caretData.index - 1]?.style
                if (!style) style = caretData.activeData.style
                if (style) {
                    caretData.style = { ...style }
                    caretData.blink = false
                    renderCaret(true)
                    setToolbarData({
                        ...caretData.style,
                        dataSetType: caretData.activeData.type,
                        listStyle: caretData.activeData.listStyle
                    })
                }

                caretData.dragging.is = true
                caretData.dragging.startX = caretPos.x
                caretData.dragging.startY = caretPos.y
                upDownArrowX = null
            }
            if (e.button == 2) {
                stopDragging()
            }
            if (caretData.dragging.is) setDragStartPosition()
        }



        function mouseMoveHandler(e) {
            if (e.button == 0 && e.buttons == 1 && caretData.dragging.is) {
                caretData.dragging.started = true
                let caretPos = getCaretPosition(e)

                caretData.dragging.endX = caretPos.x
                caretData.dragging.endY = caretPos.y
                if (caretPos) {
                    caretData.dragging.endDataIndex = caretPos.dataSetIndex
                    caretData.dragging.endIndex = caretPos.index
                    renderSelection()
                }
            }
        }


        function onFocusHandler(e) {
            focussedPage = e.target
            lastFocussedPage = e.target
            reRenderCanvas()
        }
        function onBlurHandler(e) {
            caretData.blink = true
            focussedPage = null
        }
    }

    function renderSelection(clear) {
        if (isSelectionRendering) return
        isSelectionRendering = true
        for (let p = 0; p < pageList.length; p++) {
            // to clear previous selection
            let ctx = pageList[p].selectionCanvas.getContext('2d', { willReadFrequently: true })
            ctx.clearRect(0, 0, pageList[p].selectionCanvas.width, pageList[p].selectionCanvas.height)
        }
        if (clear) {
            isSelectionRendering = false
            return
        }
        let startingChar = {}, endChar = {};
        if (caretData.dragging.startingDataIndex < caretData.dragging.endDataIndex) {
            startingChar = { dataSetIndex: caretData.dragging.startingDataIndex, index: caretData.dragging.startingCharIndex }
            endChar = { dataSetIndex: caretData.dragging.endDataIndex, index: caretData.dragging.endIndex }
        }
        else if (caretData.dragging.startingDataIndex > caretData.dragging.endDataIndex) {
            startingChar = { dataSetIndex: caretData.dragging.endDataIndex, index: caretData.dragging.endIndex }
            endChar = { dataSetIndex: caretData.dragging.startingDataIndex, index: caretData.dragging.startingCharIndex }
        } else if (caretData.dragging.startingCharIndex < caretData.dragging.endIndex) {
            startingChar = { dataSetIndex: caretData.dragging.startingDataIndex, index: caretData.dragging.startingCharIndex }
            endChar = { dataSetIndex: caretData.dragging.endDataIndex, index: caretData.dragging.endIndex }
        } else if (caretData.dragging.startingCharIndex > caretData.dragging.endIndex) {
            startingChar = { dataSetIndex: caretData.dragging.endDataIndex, index: caretData.dragging.endIndex }
            endChar = { dataSetIndex: caretData.dragging.startingDataIndex, index: caretData.dragging.startingCharIndex }
        } else {
            isSelectionRendering = false
            return
        }

        let firstLineRendered = false
        let x = 0, y = config.format.margin * pxMmRatio;
        let maxHeight = config.pageSetup.pxHeight - config.format.margin * pxMmRatio * 2
        for (let l = 0; l < lines.length; l++) {
            let lineObj = lines[l]
            x = config.format.margin * pxMmRatio
            // y = lines[l].y
            let setData = dataList[lineObj.dataIndex]
            let lineHeight = (lineObj.maxFontSize * 4 / 3) * mmPtRatio * pxMmRatio

            // if (maxHeight < y) {
            //     y = config.format.margin * pxMmRatio + (lines[l].maxFontSize * pxMmRatio * mmPtRatio);
            // }
            y = lines[l].y + (lineObj.maxFontSize / 3) * mmPtRatio * pxMmRatio
            if (lineObj.dataIndex >= startingChar.dataSetIndex && lineObj.dataIndex <= endChar.dataSetIndex) {

                let ctx = pageList[lineObj.pageIndex].selectionCanvas.getContext('2d', { willReadFrequently: true })
                ctx.save()
                ctx.fillStyle = "#0000ff55"
                let startingCharInThisLine = false, endCharInThisLine = false;
                if (
                    startingChar.dataSetIndex == lineObj.dataIndex &&
                    startingChar.index >= lineObj.startingCharIndex &&
                    startingChar.index <= lineObj.endingCharIndex

                ) {
                    startingCharInThisLine = true
                }

                if (
                    endChar.dataSetIndex == lineObj.dataIndex &&
                    endChar.index >= lineObj.startingCharIndex &&
                    endChar.index <= lineObj.endingCharIndex
                ) endCharInThisLine = true

                if (startingCharInThisLine && endCharInThisLine) {
                    let drawY = y - lineHeight
                    let drawX = setData.formatedText[startingChar.index].dimension.x
                    let midChar = setData.formatedText[endChar.index]
                    let midX = midChar?.dimension?.x
                    midX = midX ? midX : 0
                    let lastChar = setData.formatedText[lineObj.endingCharIndex]
                    let endX = lastChar?.dimension?.x;
                    endX = endX ? endX : 0
                    ctx.fillRect(drawX, drawY, midX - drawX, lineHeight)
                    break
                } else if (startingCharInThisLine) {
                    firstLineRendered = true
                    let drawY = y - lineHeight
                    let drawX = setData.formatedText?.[startingChar?.index]?.dimension?.x
                    drawX = drawX ? drawX : x
                    let lastChar = setData.formatedText[lineObj.endingCharIndex - 1]
                    let endX = lastChar?.dimension?.x + lastChar?.dimension?.width;
                    endX = endX ? endX : (drawX + lineObj.maxFontSize * mmPtRatio * pxMmRatio / 3)
                    ctx.fillRect(drawX, drawY, endX - drawX, lineHeight)
                } else if (endCharInThisLine) {
                    let drawY = y - lineHeight
                    let midChar = setData.formatedText[endChar.index]
                    let midX = midChar?.dimension?.x + midChar?.dimension?.width
                    midX = midX ? (midX) : 0
                    ctx.fillRect(x, drawY, midX - x, lineHeight)
                    break
                } else if (firstLineRendered) {
                    let drawY = y - lineHeight
                    let drawX = setData.formatedText?.[lineObj?.startingCharIndex]?.dimension?.x
                    drawX = drawX ? drawX : x
                    let lastChar = setData.formatedText[lineObj.endingCharIndex - 1]
                    let endX = lastChar?.dimension?.x + lastChar?.dimension?.width;
                    endX = endX ? endX : (drawX + lineObj.maxFontSize * mmPtRatio * pxMmRatio / 3)
                    ctx.fillRect(drawX, drawY, endX - drawX, lineHeight)
                }

                ctx.restore()
            }

        }
        isSelectionRendering = false
    }
    function stopDragging(reset) {
        if (reset) {
            caretData.dragging.startX = null
            caretData.dragging.startY = null
            caretData.dragging.endX = null
            caretData.dragging.endY = null
            caretData.dragging.startingDataIndex = null
            caretData.dragging.startingCharIndex = null
            caretData.dragging.endDataIndex = null
            caretData.dragging.endIndex = null
        }
        caretData.dragging.is = false
        caretData.dragging.started = false
        renderSelection(reset)
    }
    function setToolbarData(style) {
        let fullStyle = getFullStyle(style)

        let fontSizeInput = shadow.querySelector('[adc="font-size-input"]')
        fontSizeInput.value = fullStyle.fontSize
        let fontFamilyValue = shadow.querySelector('[adc="font-family"] [adc-type="selection"]')
        if (fullStyle.fontFamily) {
            fontFamilyValue.innerHTML = fullStyle.fontFamily
        } else {
            fontFamilyValue.innerHTML = ""
        }

        let listStyle = shadow.querySelector('[adc="list-style"] [adc-type="selection"]')
        let listStyleValue = listStyles.find(item => item.value == style.listStyle)
        if (style.dataSetType == 1 && listStyleValue) {
            listStyle.innerHTML = listStyleValue.type
        } else {
            listStyle.innerHTML = '[No-List]'
        }

        let textStyle = shadow.querySelector('[adc="style-handler"] [adc-type="selection"]')
        let textStyleValue = textStyleList.find(item => item.type == style?.type)
        if (textStyleValue) {
            textStyle.innerHTML = textStyleValue.type
        } else {
            textStyle.innerHTML = textStyleList[0]?.type
        }


    }

    function getLabel(dataIndex) {
        let label = "x."
        if (dataList[dataIndex].listStyle == 0) { // 1. a. i.
            let tabCount = dataList[dataIndex].tabCount
            tabCount = tabCount ? tabCount : 0
            if (tabCount % 3 == 0) label = `${dataList[dataIndex].listItemNumber}.`
            else if (tabCount % 3 == 1) label = `${convertToLetter(dataList[dataIndex].listItemNumber)}.`
            else label = `${convertToRoman(dataList[dataIndex].listItemNumber).toLowerCase()}.`
        }
        else if (dataList[dataIndex].listStyle == 1) { // >
            label = `>`
        }


        function convertToLetter(num) {
            num = (num - 1).toString(26)
            let label = ""
            for (let i = 0; i < num.length; i++) label += letters[num[i]]

            return label
        }

        function convertToRoman(num) {

            let result = '';
            let divisor = 1000;

            for (let i = 3; i >= 0; i--) {
                const digit = Math.floor(num / divisor);
                num %= divisor;
                divisor /= 10;

                if (digit > 0) {
                    if (digit === 9) {
                        result += romanNumerals[i][3];
                    } else if (digit >= 5) {
                        result += romanNumerals[i][2];
                        result += romanNumerals[i][0].repeat(digit - 5);
                    } else if (digit === 4) {
                        result += romanNumerals[i][1];
                    } else {
                        result += romanNumerals[i][0].repeat(digit);
                    }
                }
            }

            return result;
        }

        return label
    }

    function changeListStyle() {
        let activeDataIndex = dataList.findIndex(item => item.id == caretData.activeData.id)
        let d = activeDataIndex - 1

        while (dataList?.[d]?.type == 1) {
            dataList[d].listStyle = caretData.activeData.listStyle
            --d
        }
        d = activeDataIndex + 1
        while (dataList?.[d]?.type == 1) {
            dataList[d].listStyle = caretData.activeData.listStyle
            ++d
        }
    }




    function addFonts(paths, name) {
        if (typeof paths == 'string') paths = [paths]
        let fontObj = {
            paths: [],
            name: name,
        }
        if (!isModule) {

            let linkString = ''
            for (let i = 0; i < paths.length; i++) {
                let format = paths[i].split('.');
                format = format[format.length - 1]
                fontObj.paths.push(paths[i])
                linkString += `url(${paths[i]}) format("${format == 'ttf' ? 'truetype' : format}")${(i >= paths.length - 1) ? '' : ',\n'}`

                if (format == 'ttf') {
                    fetch(paths[i])
                        .then(response => response.arrayBuffer())
                        .then(fontBytes => { fontObj.fontBytes = fontBytes })
                }
            }
            const customFont = new FontFace(`${name}`, `${linkString}`);
            customFont.load()
                .then((loadedFont) => {
                    document.fonts.add(loadedFont);
                    loadedFont.loaded.then(() => {
                        fontList.push(fontObj)
                        reRenderFontDropdown()
                    })
                })

        }

        function reRenderFontDropdown() {
            let fontFamilyDropdown = shadow.querySelector('[adc="font-family"]')
            if (fontFamilyDropdown) {
                fontFamilyDropdown.innerHTML = ""
                let selectionOption = document.createElement('span')
                selectionOption.setAttribute('adc-type', 'selection')
                let caretStyle = getFullStyle(caretData.style)
                selectionOption.innerHTML = caretStyle.fontFamily
                fontFamilyDropdown.append(selectionOption)

                let ul = document.createElement('ul')
                ul.setAttribute('class', 'dropdown')
                fontFamilyDropdown.append(ul)
                fontList.forEach((font, i) => {
                    let optionTag = document.createElement('li')
                    optionTag.setAttribute('value', font.name)
                    optionTag.style.fontFamily = font.name
                    optionTag.innerText = font.name
                    ul.append(optionTag)
                    optionTag.addEventListener('click', () => {
                        let caretStyle = getFullStyle(caretData.style)
                        caretData.style = {
                            ...caretStyle,
                            fontFamily: font.name,
                        }
                        delete caretData.style.type
                        focusOnPage()
                        // selectionOption.style.fontFamily = font.name
                    })
                })
                // selectionOption.style.fontFamily = caretStyle.fontFamily
            }


        }

    }


    function onMouseWheelHandler(e) {
        if (e.ctrlKey || e.metaKey) e.preventDefault()
    }

    function addGlobalEvents(e) {
        shadow.querySelector('[adc="page-list"]').addEventListener('wheel', onMouseWheelHandler)
        shadow.addEventListener('click', globalClickDownHandler)
        shadow.addEventListener('mouseover', globalMouseOverHandler)
        shadow.addEventListener('mouseup', globalMouseUpHandler)
    }

    function globalMouseUpHandler(e) {
        caretData.dragging.is = false
        console.log('drag', caretData.dragging)
    }

    function globalClickDownHandler(e) {

        // these is to handle the dropdown items
        let target;
        let elem = e.target
        let selection
        if (elem?.tagName == 'LI') {
            selection = elem?.textContent
        }
        while (elem) {
            if (elem?.tagName == 'DIV' && elem?.classList?.contains('item')) {
                target = elem.querySelector('ul.dropdown');
                break;
            }
            elem = elem?.parentNode
        }
        if (target) {
            if (selection) {
                let selectionOption = elem.querySelector('[adc-type="selection"]')
                selectionOption.innerHTML = selection
            }
            target?.classList?.toggle('show')
        }
        let ulElem = shadow.querySelectorAll('ul.dropdown')
        ulElem.forEach(item => {
            if (target != item) item?.classList?.remove('show')
        })


    }
    function findSpecialCharacterIndex(str, startingCharIndex, forward) {
        let delta = forward ? 1 : -1
        let curIndex = startingCharIndex + delta
        while (str[curIndex]) {
            if (str[curIndex] == " ") return curIndex
            curIndex += delta
        }
        return null
    }

    function globalMouseOverHandler(e) {
        let menuItems = shadow.querySelectorAll('[adc-type="menu-item"]')
        menuItems.forEach(item => item.classList.remove('hover'))
        let hoverElem
        let elem = e?.target
        while (elem) {
            if (elem?.getAttribute?.('adc-type') == 'menu-item') {
                hoverElem = elem;
                break
            }
            elem = elem.parentNode
        }
        if (hoverElem) {
            hoverElem.classList.add('hover')
        }
    }

    function getCaretStyle(customStyle) {
        let style = { type: textStyleList[0].type }
        if (customStyle) style = { ...customStyle }
        else if (caretData.activeData) {
            if (caretData?.activeData?.formatedText?.[caretData.index - 1]) {
                style = { ...caretData.activeData.formatedText[caretData.index - 1]?.style }
            } else {
                style = { ...caretData.activeData.style }
            }
        }
        return style
    }

    function setCaretStyle(customStyle) {
        caretData.style = { ...getCaretStyle(customStyle) }
        return caretData.style
    }

    function getFullStyle(refStyle) {
        let style = {}
        if (refStyle.type) {
            style = { ...textStyleList[styleName[refStyle.type]] }
        }
        style = { ...style, ...refStyle }
        style = JSON.parse(JSON.stringify(style))
        if (Object.keys(style).length > 1) delete style.type
        return style
    }


    function renderCaret(toLoop) {
        clearTimeout(caretData.timeout)
        let dataIndex = dataList.findIndex(item => item.id == caretData.activeData.id)
        let lineIndex = lines.findIndex(item => item.dataIndex == dataIndex && caretData.index <= item.endingCharIndex)
        let lineObj = lines[lineIndex]
        let prevLine = lines[lineIndex - 1]

        let ctx = pageList[lineObj.pageIndex].el.getContext('2d', { willReadFrequently: true })
        ctx.save()
        if (caretData.previousCaret) {
            ctx.putImageData(caretData.previousCaret.imageData, caretData.previousCaret.x, caretData.previousCaret.y);
            caretData.previousCaret = null
        }
        if (!caretData.blink) {
            let dataIndex = dataList.findIndex(item => item.id == caretData.activeData.id)
            let lineObj = lines.find(item => item.dataIndex == dataIndex && caretData.index >= item.startingCharIndex)
            let x = (config.format.margin * pxMmRatio) + lineObj.tabWidth
            let y = (config.format.margin) * pxMmRatio

            let caretStyle = getFullStyle(caretData.style)
            let height = caretStyle.fontSize * pxMmRatio * mmPtRatio * 5 / 4
            let width = height / 10

            let charDimension = caretData.activeData.formatedText[caretData.index - 1]?.dimension
            if (lineObj) {
                x = (charDimension ? (charDimension.x + charDimension.width) : x)
                if (charDimension) {
                    y = charDimension.y - (caretStyle.fontSize * pxMmRatio * mmPtRatio)
                } else {
                    if (prevLine && prevLine.pageIndex != lineObj.pageIndex) {
                        y = (config.format.margin) * pxMmRatio
                    }
                    else {
                        y = lineObj.y - (lineObj.maxFontSize * pxMmRatio * mmPtRatio)
                    }
                }
            }


            const imageData = ctx.getImageData(x, y, width, height);
            const data = imageData.data;
            caretData.previousCaret = { imageData: structuredClone(imageData), x, y }
            // Invert the color of the rectangular region
            for (let i = 0; i < data.length; i += 4) {
                data[i] = 255 - data[i]; // Red
                data[i + 1] = 255 - data[i + 1]; // Green
                data[i + 2] = 255 - data[i + 2]; // Blue
                // Alpha channel remains unchanged (data[i + 3])
            }
            ctx.putImageData(imageData, x, y);
            caretData.blink = true
        } else {
            caretData.blink = false
        }

        let thisPage = pageList[lineObj.pageIndex].el
        if (thisPage != focussedPage) {
            lastFocussedPage = thisPage
            focussedPage = thisPage
            // focusOnPage()
        }

        ctx.restore()
        if (toLoop) {
            caretData.timeout = setTimeout(() => {
                renderCaret(true)
            }, caretData.timeoutDuration)
        }
        return
    }
    function focusOnPage(options = { scroll: false }) {
        caretData.blink = false
        // if (!lastFocussedPage) lastFocussedPage = pageList[0].el
        // if (lastFocussedPage) {
        //     const scrollTop = pageScrollingDiv.scrollTop
        //     // lastFocussedPage.focus(); // comment this
        //     pageScrollingDiv.scrollTop = scrollTop
        //     if (options.scroll) {
        //         lastFocussedPage.scrollIntoView({ behavior: "smooth" })
        //     }
        // }
    }

    function newDocument(e) {
        globalMouseOverHandler(); // to hide the menu when clicked
        caretData.blink = true
        dataList = [
            {
                id: ++counter,
                type: 0,
                formatedText: [],
                plainContent: "",
                tabCount: 0,
                style: { ...config.style },
                listItemNumber: 0,
            },
        ]

        caretData = {
            activeData: dataList[0],
            index: 0,
            timeout: null,
            timeoutDuration: 800,
            blink: true,
            pageIndex: 0,
            caretSize: config.style.fontSize,
            previousCaret: null,
            style: { ...dataList[0].style },
            dimension: {
                x: config.format.margin,
                y: config.format.margin + (3 * config.style.fontSize / 4)
            },
            dragging: {
                is: false, // to check if mouse is down for dragging,
                started: false, // to check if the mouse has moved for dragging
                startX: null, // start and end are coordinated of opposite corner
                startY: null,
                endX: null,
                endY: null,
                startingDataIndex: null,
                endDataIndex: null,
                startingCharIndex: null,
                endIndex: null,
            }
        }
        pageList = []
        shadow.querySelector('[adc="page-list"').innerHTML = ""
        reRenderCanvas()
    }

    function downloadJSON() {
        let data = compressDocJSON()
        let jsonString = JSON.stringify(data)
        const blob = new Blob([jsonString], { type: 'application/json' });

        const a = shadow.querySelector('[adc="download-link"');
        a.download = 'a-saved-data.json';
        a.href = URL.createObjectURL(blob);
        document.body.appendChild(a);
        a.click();


    }

    function compressDocJSON() {
        let activeDataIndex = dataList.findIndex(item => item.id == caretData.activeData.id)
        let returnObj = {
            lines: JSON.parse(JSON.stringify(lines)), // this is not required by the user.
            pxMmRatio: pxMmRatio,
            config: JSON.parse(JSON.stringify(config)),
            content: JSON.parse(JSON.stringify(dataList)),
            caretData: JSON.parse(JSON.stringify(caretData)),
            pageList: pageList,
        }

        delete returnObj.caretData.activeData
        delete returnObj.caretData.previousCaret
        returnObj.caretData.activeDataIndex = activeDataIndex
        returnObj.compressed = true

        return returnObj
    }


    function setDocumentData(data) {
        // might need to change some configuration
        for (let d = 0; d < data.content.length; d++) data.content[d].id = ++counter
        dataList = JSON.parse(JSON.stringify(data.content))
        config = JSON.parse(JSON.stringify(data.config))
        caretData = JSON.parse(JSON.stringify(data.caretData))
        caretData.activeData = dataList[data.caretData.activeDataIndex]
        caretData.blink = false
        reRenderCanvas()
    }

    async function printDoc() {
        // pdf dimention in pdf-lib is based on 'pt' or 'points' by default.
        // 1pt = 1/72 inch

        const { PDFDocument, rgb } = PDFLib

        const pdfDoc = await PDFDocument.create();
        pdfDoc.registerFontkit(fontkit)
        let embeddedFonts = {}

        let page;
        let pageHeight = config.pageSetup.mmHeight / mmPtRatio
        let pageWidth = config.pageSetup.mmWidth / mmPtRatio
        let x = 0, y = config.format.margin * pxMmRatio;
        for (let l = 0; l < lines.length; l++) {
            let dataSet = dataList[lines[l].dataIndex]
            if (lines?.[l - 1]?.pageIndex != lines[l].pageIndex) {
                page = pdfDoc.addPage([pageWidth, pageHeight]);
            }
            x = config.format.margin * pxMmRatio
            x += lines[l].tabWidth

            // this is to render the numbering and bullets etc
            if (lines[l].dataType == 1) {
                let fontObj = fontList.find(item => item.name == dataSet.style.fontFamily)
                let font = embeddedFonts[fontObj.name]
                if (!font) {
                    const fontBytes = fontObj.fontBytes
                    font = await pdfDoc.embedFont(fontBytes);
                    embeddedFonts[fontObj.name] = font
                }
                let label = getLabel(lines[l].dataIndex)
                let width = lines[l].labelWidth

                x = (x - width) / (pxMmRatio * mmPtRatio)
                y = pageHeight - lines[l].y / (pxMmRatio * mmPtRatio)
                page.drawText(label, {
                    x, y, font,
                    size: dataSet.style.fontSize,
                    color: rgb(...getRgb(dataSet.style.fontColor))
                });
            }


            for (let c = lines[l].startingCharIndex; c < lines[l].endingCharIndex; c++) {
                let fontObj = fontList.find(item => item.name == dataSet.formatedText[c]?.style?.fontFamily)
                let font = embeddedFonts[fontObj.name]
                if (!font) {
                    const fontBytes = fontObj.fontBytes
                    font = await pdfDoc.embedFont(fontBytes);
                    embeddedFonts[fontObj.name] = font
                }

                x = dataSet.formatedText[c]?.dimension?.x / (pxMmRatio * mmPtRatio); // X-coordinate
                y = pageHeight - dataSet.formatedText[c]?.style?.y / (pxMmRatio * mmPtRatio); // Y-coordinate

                page.drawText(dataSet.plainContent[c], {
                    x, y, font,
                    size: dataSet.formatedText[c]?.style?.fontSize,
                    color: rgb(...getRgb(dataSet.formatedText[c]?.style.fontColor))
                });

            }

            // Set the font and size
            // Embed the custom font




        }
        function getRgb(hex) {
            let rgbArr = [0, 0, 0]
            if (hex.length == 7) {
                rgbArr = [parseInt(hex.slice(1, 3), 16) / 255, parseInt(hex.slice(3, 5), 16) / 255, parseInt(hex.slice(5, 7), 16) / 255]
            } else if (hex.length == 4) {
                rgbArr = [parseInt(hex.slice(1, 2), 16) / 15, parseInt(hex.slice(2, 3), 16) / 15, parseInt(hex.slice(3, 4), 16) / 15]
            }
            return rgbArr
        }



        // Save the modified PDF
        const pdfBytes = await pdfDoc.save();
        // For demonstration purposes, you can download the PDF
        const blob = new Blob([pdfBytes], { type: 'application/pdf' });
        const link = shadow.querySelector('[adc="download-link"]');
        link.href = URL.createObjectURL(blob);
        link.download = '1_from_editor.pdf';
        link.click();
    }

    async function setupHeaders() {
        let headerList = shadow.querySelector('[adc="left-sidebar-content"]')
        headerList.innerHTML = ""
        for (let d = 0; d < dataList.length; d++) {
            let styleIndex = textStyleList.findIndex(item => item.type == dataList[d]?.style?.type)
            if (styleIndex > 0 && dataList[d].plainContent.length) {
                let headingItem = document.createElement('div')
                headingItem.innerHTML = dataList[d].plainContent
                headingItem.innerHTML = "&nbsp;".repeat(textStyleList[styleIndex].spaces*3) + headingItem.innerHTML
                headerList.append(headingItem)
                // let headingStyle = getFullStyle(dataList[d].style)
                // headingItem.style.fontSize = `${headingStyle.fontSize / 1.2}pt`
                headingItem.style.width = '100%'
                headingItem.style.whiteSpace = 'nowrap'
                headingItem.style.overflow = "hidden"
                headingItem.style.textOverflow = "ellipsis"
            }
        }
    }

    var returnObj = {
        getDocumentData() {
            return compressDocJSON()
        },
        setDocumentData,
        addFonts,
        getConfiguration() { return JSON.parse(JSON.stringify(config)) },
        getPages() {
            let pagesToReturn = JSON.parse(JSON.stringify(pageList))
            for (let i = 0; i < pagesToReturn.length; i++) pagesToReturn[i].canvas = pageList[i].canvas
            return pagesToReturn
        },
        log() {
            console.log(compressDocJSON())
        },
        focusOnPage,
        printDoc,
    }

    initialize()
    return returnObj
}


if (isModule) module.exports = ADocEditor