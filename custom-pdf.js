const router = require("express").Router()
const path = require('path')
let ADocEditor = require("./dist/assets/a-doc-editor.js")

var inProgress = false
let fileName = path.join( __dirname, "temporary", "temp.pdf" )
router.post( "/generate", async function(req, res){
    if (inProgress){
        console.log('server busy')
        res.status(200).json({ success: false, message: "Server Busy" })
        return
    }
    try{
        inProgress = true
        console.clear()
        let editor = new ADocEditor()
        console.log('editor', editor)
        let result = await editor.generatePDF(fileName)
        console.log('result:', result)
        inProgress = false
        res.status(200).json( { success: true, message: "Successfull", fileName: fileName } )
    }catch(err){
        console.log(err)
        inProgress = false
        res.status(200).json({ message: "Failed" })
    }
} )

router.get("/display", async function(req, res){
    try{
        console.log('reaching')
        res.sendFile( fileName )
    }catch(err){
        console.log(err)
        res.status(200).json({ message: "Failed" })
    }
})

module.exports = router