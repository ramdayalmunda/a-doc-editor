const express = require("express")
const port = 3910;
const path = require("path");
const app = express()
app.use(express.json())

app.use('/pdf', require("./custom-pdf"))
app.use(express.static(path.join(__dirname, 'dist')))



app.listen(port, () => {
    console.log(`Doc Editor on http://localhost:${port}`)
})

